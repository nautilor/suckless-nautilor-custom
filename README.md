# NAUTILOR SUCKLESS

This is my suckeless configurations

## DWM

	- gaps			 	
	- centermaster 
	- restartsig
	- systray

## ST

	- scrollback (only keyboard)

## DMENU

	- Xresources
	- center dmenu
	- modified padding manually
	- modified width manually
	- fuzzy finder
	- border

to change the padding just find the line

		bh = MAX(bh, 30);

and change the `30` with whatever you want

for the width find the lines

	mw = MIN(MAX(max_textw() + promptw, 750), info[i].width);
	w = MIN(MAX(max_textw() + promptw, 750), wa.width);

and change the `750` with whatever you want
